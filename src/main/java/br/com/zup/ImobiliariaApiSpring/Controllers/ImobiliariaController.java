package br.com.zup.ImobiliariaApiSpring.Controllers;

import br.com.zup.ImobiliariaApiSpring.Services.ImobiliariaService;
import br.com.zup.ImobiliariaApiSpring.Services.ImovelService;
import br.com.zup.ImobiliariaApiSpring.dtos.ImovelDTO;
import br.com.zup.ImobiliariaApiSpring.exceptions.EndNaoCadastradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/imobiliaria")
public class ImobiliariaController {

    @Autowired
    ImobiliariaService imobiliaria;

    @Autowired
    ImovelService imovelService;

    @PostMapping
    public void recebeImovel(@RequestBody @Valid ImovelDTO imovelDTO){
        imobiliaria.addImovel(imovelDTO);
    }

    @GetMapping
    public List<ImovelDTO> chamaListaImoveis(){
        return imobiliaria.exibirListaImoveis();
    }

    @GetMapping("/{endereco}/{cpf}")
    public void chamaExluirMorador(@PathVariable String endereco, @PathVariable String cpf){
        ImovelDTO imovelDTO;
        try{
            imovelDTO = imobiliaria.verificaEndExiste(endereco);
            imovelService.removerMoradorCpf(imovelDTO, cpf);
        } catch (EndNaoCadastradoException e){
            e.getMessage();
        }
    }

}
