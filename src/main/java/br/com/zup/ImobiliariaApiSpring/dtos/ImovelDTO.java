package br.com.zup.ImobiliariaApiSpring.dtos;

import br.com.zup.ImobiliariaApiSpring.exceptions.MoradorNaoCadastradoException;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class ImovelDTO {
    private String codigo;
    private String endereco;
    private double valorAluguel;
    private FuncionarioDTO funcionarioDTO;
    private List<MoradorDTO> listaMoradores = new ArrayList<>();

    public ImovelDTO() {}

    @Size(min = 3, message = "{validacao.codigo}")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @NotBlank(message = "{validacao.blank}")
    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    @Min(value = 250, message = "{validacao.valor.aluguel}")
    public double getValorAluguel() {
        return valorAluguel;
    }

    public void setValorAluguel(double valorAluguel) {
        this.valorAluguel = valorAluguel;
    }

    @Valid
    public FuncionarioDTO getFuncionarioDTO() {
        return funcionarioDTO;
    }

    public void setFuncionarioDTO(FuncionarioDTO funcionarioDTO) {
        this.funcionarioDTO = funcionarioDTO;
    }

    @Valid
    public List<MoradorDTO> getListaMoradores() {
        return listaMoradores;
    }

    public void setListaMoradores(List<MoradorDTO> listaMoradores) {
        this.listaMoradores = listaMoradores;
    }

}
