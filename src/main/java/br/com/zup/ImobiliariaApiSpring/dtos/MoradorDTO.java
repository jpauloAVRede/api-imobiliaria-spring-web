package br.com.zup.ImobiliariaApiSpring.dtos;

import javax.validation.constraints.Email;

public class MoradorDTO extends PessoaDTO{
    private String email;

    public MoradorDTO() {}

    @Email(message = "{validacao.email}")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
