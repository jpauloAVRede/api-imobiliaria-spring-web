package br.com.zup.ImobiliariaApiSpring.dtos;

import javax.validation.constraints.*;

public class FuncionarioDTO extends PessoaDTO{
    private String matricula;
    private String funcao;
    private double salario;

    private FuncionarioDTO() {}

    @Size(min = 3, message = "{validacao.matricula}")
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @NotBlank(message = "{validacao.blank}")
    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    @Min(value = 1150)
    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

}
