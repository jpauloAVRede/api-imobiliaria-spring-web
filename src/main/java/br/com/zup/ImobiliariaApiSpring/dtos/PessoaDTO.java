package br.com.zup.ImobiliariaApiSpring.dtos;

import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class PessoaDTO {
    private String nome;
    private String cpf;
    private String telefone;

    public PessoaDTO() {}

    @Size(min = 3, message = "{validacao.nome}")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @CPF(message = "{validacao.cpf}")
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Size(min = 10, max = 11, message = "{validacao.telefone}")
    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

}
