package br.com.zup.ImobiliariaApiSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImobiliariaApiSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImobiliariaApiSpringApplication.class, args);
	}

}
