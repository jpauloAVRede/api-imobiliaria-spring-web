package br.com.zup.ImobiliariaApiSpring.exceptions;

public class EndNaoCadastradoException extends RuntimeException{

    public EndNaoCadastradoException(String message) {
        super(message);
    }
}
