package br.com.zup.ImobiliariaApiSpring.exceptions;

public class MoradorNaoCadastradoException extends RuntimeException{

    public MoradorNaoCadastradoException(String message){
        super(message);
    }
}
