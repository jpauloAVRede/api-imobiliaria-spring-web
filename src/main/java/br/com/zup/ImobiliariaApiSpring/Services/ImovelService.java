package br.com.zup.ImobiliariaApiSpring.Services;

import br.com.zup.ImobiliariaApiSpring.dtos.ImovelDTO;
import br.com.zup.ImobiliariaApiSpring.dtos.MoradorDTO;
import br.com.zup.ImobiliariaApiSpring.exceptions.MoradorNaoCadastradoException;
import org.springframework.stereotype.Service;

@Service
public class ImovelService {

    public MoradorDTO removerMoradorCpf(ImovelDTO imovelDTO, String cpf){
        MoradorDTO moradorExcluir = null;
        for (MoradorDTO elemLista : imovelDTO.getListaMoradores()){
            if (elemLista.getCpf().equals(cpf)){
                moradorExcluir = elemLista;
            }
        }
        if (moradorExcluir != null){
            imovelDTO.getListaMoradores().remove(moradorExcluir);
            return moradorExcluir;
        }
        throw new MoradorNaoCadastradoException("Morador não cadastrado!!!");
    }

}
