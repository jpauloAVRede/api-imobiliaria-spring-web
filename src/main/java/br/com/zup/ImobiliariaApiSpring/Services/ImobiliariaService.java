package br.com.zup.ImobiliariaApiSpring.Services;

import br.com.zup.ImobiliariaApiSpring.dtos.ImovelDTO;
import br.com.zup.ImobiliariaApiSpring.exceptions.EndNaoCadastradoException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImobiliariaService {

    private List<ImovelDTO> listaImoveis = new ArrayList<>();

    public void addImovel(ImovelDTO imovelDTO){
        listaImoveis.add(imovelDTO);
    }

    public List<ImovelDTO> exibirListaImoveis(){
        return this.listaImoveis;
    }

    public ImovelDTO verificaEndExiste(String endereco){
        ImovelDTO imovelDTO = null;
        for (ImovelDTO elemLista : listaImoveis){
            if (elemLista.getEndereco().equals(endereco)){
                imovelDTO = elemLista;
            }
        }

        if (imovelDTO != null){
            return imovelDTO;
        }
        throw new EndNaoCadastradoException("Endereço não existe!!!");
    }

}
